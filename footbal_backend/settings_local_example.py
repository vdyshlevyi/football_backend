DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': 'football_backend',
        'USER': '<user>',
        'PASSWORD': '<user_password>',
        'HOST': 'localhost',
        'PORT': '5432',
        'STORAGE_ENGINE': 'INNODB'
    }
}
