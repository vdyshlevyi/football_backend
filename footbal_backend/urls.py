from django.contrib import admin
from django.conf import settings
from django.urls import include, path

import debug_toolbar


urlpatterns = [
    path('admin/', admin.site.urls),

    path('api/v1/accounts/', include(('apps.accounts.urls', 'accounts', ), namespace='accounts')),
    path('api/v1/teams/', include(('apps.teams.urls', 'teams', ), namespace='teams')),
]


if settings.DEBUG:
    urlpatterns += [
        path('__debug__/', include(debug_toolbar.urls))
    ]
