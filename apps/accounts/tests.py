from django.urls.base import reverse
from rest_framework import status
from rest_framework.test import APIClient, APITestCase

from . import factory
from . import models


class AccountTests(APITestCase):
    def setUp(self):
        self.client = APIClient()

    def tearDown(self):
        pass

    def response_check(self, response, status_code):
        if not response.status_code == status_code:
            print(response.json())
        self.assertEqual(response.status_code, status_code)

    def test_user_registration_and_login(self):
        url = reverse('accounts:registration')
        registration_data = factory.UserFactory().mock_data
        response = self.client.post(url, data=registration_data)
        self.response_check(response, status.HTTP_201_CREATED)

        user = models.User.objects.filter(username=registration_data.get('username')).first()
        self.assertIsNotNone(user)

        url = reverse('accounts:login')
        login_data = {
            'username': registration_data['username'],
            'password': registration_data['password'],
        }
        response = self.client.post(url, data=login_data)
        self.response_check(response, status.HTTP_200_OK)

    def test_reset_password(self):
        user = factory.UserFactory().create()

        url = reverse('accounts:reset-password')
        reset_data = {'username': user.username}

        response = self.client.post(url, data=reset_data)
        self.response_check(response, status.HTTP_200_OK)
