from django.db.models.query_utils import Q
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from apps.accounts.models import User
from . import models


class RegistrationSerializer(serializers.ModelSerializer):
    password = serializers.CharField(min_length=6, max_length=50, write_only=True)
    password_confirm = serializers.CharField(min_length=6, max_length=50, write_only=True)

    class Meta:
        model = models.User
        fields = (
            'username',
            'email',
            'first_name',
            'last_name',
            'email',
            'password',
            'password_confirm',
        )

    def validate(self, attrs):
        password, password_confirm = attrs['password'], attrs.pop('password_confirm', None)
        if password != password_confirm:
            raise ValidationError(_('Password mismatch.'))
        return attrs

    def create(self, data):
        return models.User.objects.create_user(**data)


class ResetPasswordSerializer(serializers.Serializer):
    email = serializers.EmailField(required=False, allow_null=True, allow_blank=True)
    username = serializers.CharField(max_length=150, required=False, allow_null=True, allow_blank=True)

    def create(self, data):

        email, username = data.get('email'), data.get('username')

        if username and email:
            raise ValidationError(_("Provide username or email, not both."))
        elif not username and not email:
            raise ValidationError(detail=_("Provide username or email."))

        user = User.objects.filter(Q(email=email) | Q(phone_number=username)).first()
        if not user:
            raise ValidationError(_("User with this email/username doesn't exist"))

        # TODO -> send reset email here
        # from django.core.mail import send_mail
        #
        # send_mail(
        #     subject='Subject here',
        #     message='Here is the message.',
        #     from_email='from@example.com',
        #     recipient_list=['to@example.com'],
        #     fail_silently=False,
        # )
        return {'result': _('Reset instruction was send on your email.')}
