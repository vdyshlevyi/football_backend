import random

from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import Permission
from django.conf import settings
from factory.base import use_strategy
from factory import enums

from . import models

from faker import Factory, factory


fake = Factory.create()


class UserFactory(object):
    def __init__(self, *args, **kwargs):
        password = fake.password()
        self.mock_data = {
            'first_name': fake.first_name(),
            'last_name': fake.last_name(),
            'email': fake.profile()['mail'],
            'username': self.__unique_username(),
            'password': password,
            'password_confirm': password,
        }

    def __unique_username(self):
        username, username_exists = None, True
        while username_exists:
            username = fake.user_name()
            username_exists = models.User.objects.filter(username=username).exists()
        return username

    def create(self):
        self.mock_data.pop('password_confirm', None)
        return models.User.objects.create_user(**self.mock_data)
