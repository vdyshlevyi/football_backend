from django.contrib.auth.models import BaseUserManager, AbstractUser
from django.db import models
from django.utils.translation import ugettext_lazy as _


class UserManager(BaseUserManager):
    def create_user(self, username, email, password=None, **other):
        user = self.model(email=self.normalize_email(email), username=username, **other)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, username, password):
        user = self.create_user(
            username=username,
            email=email,
            password=password,
            is_staff=True,
            is_superuser=True,
            first_name='Admin',
            last_name='Adminski',
        )
        return user


class User(AbstractUser):
    avatar = models.ImageField(upload_to='avatars', null=True, blank=True)
    address = models.CharField(max_length=500, null=True, blank=True)
    phone = models.CharField(max_length=15, null=True, blank=True)

    objects = UserManager()

    class Meta:
        verbose_name = _('User')
        verbose_name_plural = _('Users')

    def __str__(self):
        return self.username

    @property
    def has_token(self):
        return hasattr(self, 'auth_token')
