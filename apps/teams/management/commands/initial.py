from django.core.management.base import BaseCommand

from apps.teams import models


class Command(BaseCommand):
    def handle(self, *args, **options):
        countries = {
            'England': {
                'name_short': 'en',
                'leagues': ['Premier League', 'Championship']
            },
            'Spain': {
                'name_short': 'es',
                'leagues': ['La Liga', 'Segunda División']
            },
            'Ukraine': {
                'name_short': 'ua',
                'leagues': ['Premier League']
            },
            'Germany': {
                'name_short': 'de',
                'leagues': ['Bundesliga', '2. Bundesliga']
            },
            'Italy': {
                'name_short': 'it',
                'leagues': ['Serie A', 'Serie B']
            }
        }
        for country_name in countries:
            country, created = models.Country.objects.get_or_create(
                name=country_name,
                name_short=countries[country_name]['name_short']
            )
            if created:
                print('{} was created.'.format(country))
            else:
                print('{} already exists.'.format(country))
            for league_name in countries[country.name]['leagues']:
                league, created = models.League.objects.get_or_create(
                    name=league_name,
                    country=country
                )
                print(league)
                if created:
                    print('{} was created.'.format(league))
                else:
                    print('{} already exists.'.format(league))
            print('='*50)
