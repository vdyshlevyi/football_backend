from django.core.management.base import CommandError

from apps.teams import models
from apps.teams.commands import BS4Command
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class Command(BS4Command):
    def handle(self, *args, **options):
        driver = webdriver.Chrome(executable_path='/Users/valera/Downloads/chromedriver')
        driver.get('http://agroportal.ua/ua/publishing/konkurs/final-vseukrainskogo-konkursa-luchshaya-agrarnaya-praktika-2018-golosuem/')
        try:
            WebDriverWait(driver, 10)
        finally:
            elem = driver.find_element_by_name('iframe')

            print(elem)
            driver.close()

        # # Load page with all leagues and countries
        # country_soup = self.get_soup(part='/leagues')
        #
        # for league in models.League.objects.select_related('country')[:1]:
        #     selector = f'#{league.country.name.lower()} > div > table > tr > td:nth-of-type(1) > a'
        #     for a_league in country_soup.select(selector=selector):
        #         league_name = a_league.text.strip()
        #         if league_name == league.name:
        #             print(a_league)
        #             league_soup = self.get_soup(part=f"{a_league.attrs.get('href')}")
        #             if league_soup:
        #                 self.parse_league_teams(league_soup=league_soup)

    def parse_league_teams(self, league_soup):
        for a_team in league_soup.select(selector='.full-league-table > tbody > tr > td:nth-of-type(3) > a')[:1]:
            team_href = a_team.attrs.get('href')
            team_soup = self.get_soup(part=team_href)
            if team_soup:
                return self.parse_team_players(team_soup=team_soup)

    def parse_team_players(self, team_soup):
        players = []
        for a_player in team_soup.select('a[itemprop="name"]'):
            player_soup = self.get_soup(a_player.attrs.get('href'))
            if player_soup:
                print('->', a_player.attrs.get('href'))
                player_data = self.parse_player_data(player_soup=player_soup)
                if player_data:
                    print(player_data)
                    players.append(player_data)
                else:
                    # parse name and country
                    pass
        return players

    def parse_player_data(self, player_soup):
        player_list_info = player_soup.select('#teamSummary > div:nth-of-type(1) > div > div > p:nth-of-type(2)')
        if len(player_list_info) < 4:
            return
        return {
            'full_name': player_list_info[0].text,
            'country': player_list_info[1].text,
            'birthday': player_list_info[3].text
        }





        # index = 0
        # for i in soup.select('script[type="text/javascript"]'):
        #     if 'standings' not in i.text[:300]:
        #         continue
        #     print(type(i))
        #     text_data = i.text
        #     start = text_data.find("DataStore.prime('standings',")
        #     if start >= 0:
        #         text_data = text_data[start:]
        #     end = text_data.find(')')
        #     if end >= 0:
        #         text_data = text_data[:end]
        #
        #     print(text_data)
        #
        #     import re
        #     pattern = re.compile('\"(teamName|opponentTeam|opponentTeamName)\":\"(\w+\s?\w+)\"')
        #     # b = set(re.findall(pattern, text_data))
        #     b = set([i[1] for i in set(re.findall(pattern, text_data))])
        #
        #     print(b)
        #     print(len(b))
        #     # print(text_data)
        #     print(start)
        #     print(end)
        #
        #
        #     print('='*50, index)
        #     index += 1
        # for table_tr in soup.select('td.team'):
        #     print(table_tr)

