import requests

from bs4 import BeautifulSoup

from django.core.management.base import BaseCommand


class BS4Command(BaseCommand):
    BASE_URL = 'https://footystats.org{}'

    def get_soup(self, part=''):
        response = requests.get(
            url=self.BASE_URL.format(part),
            timeout=10,
            headers={
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) '
                                   'AppleWebKit/537.36 (KHTML, like Gecko) '
                                   'Chrome/70.0.3538.77 Safari/537.36',
            }
        )
        print(response.status_code)
        return BeautifulSoup(response.content, 'html.parser') if response.status_code == 200 else None
