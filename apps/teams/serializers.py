from rest_framework import serializers

from . import models


class CountrySerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Country
        fields = (
            'id',
            'name',
            'name_short',
        )


class LeagueSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.League
        fields = (
            'id',
            'name',
            'country',
            'members_number',
        )


class TeamSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Team
        fields = (
            'id',
            'country',
            'leagues',
            'name',
        )


class PlayerSerializer(serializers.ModelSerializer):
    model = models.Player
    fields = (
        'id',
        'first_name',
        'last_name',
    )
