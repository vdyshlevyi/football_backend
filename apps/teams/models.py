from django.db import models
from django.utils.translation import ugettext_lazy as _


class Country(models.Model):
    name = models.CharField(max_length=50)
    name_short = models.CharField(max_length=2, null=True, blank=True)

    class Meta:
        verbose_name = _('Country')
        verbose_name_plural = _('Countries')

    def __str__(self):
        return self.name


class League(models.Model):
    name = models.CharField(max_length=50)
    country = models.ForeignKey(
        Country,
        related_name='country_leagues',
        null=True, blank=True,
        on_delete=models.CASCADE
    )
    members_number = models.PositiveIntegerField(default=0)

    class Meta:
        verbose_name = _('League')
        verbose_name_plural = _('Leagues')

    def __str__(self):
        return self.name


class LeagueTeam(models.Model):
    league = models.ForeignKey('teams.League', related_name='league_teams', on_delete=models.CASCADE)
    team = models.ForeignKey('teams.Team', related_name='team_leagues', on_delete=models.CASCADE)

    class Meta:
        verbose_name = _('LeagueTeam')
        verbose_name_plural = _('LeaguesTeams')

    def __str__(self):
        return 'LeagueTeam {}-{}'.format(self.league_id, self.team_id)


class Team(models.Model):
    country = models.ForeignKey(Country, related_name='country_teams', on_delete=models.CASCADE)
    leagues = models.ManyToManyField(League, through=LeagueTeam)
    name = models.CharField(max_length=50)

    class Meta:
        verbose_name = _('Team')
        verbose_name_plural = _('Teams')

    def __str__(self):
        return self.name


class Player(models.Model):
    league = models.ForeignKey(League, related_name='league_players', on_delete=models.CASCADE)
    team = models.ForeignKey(Team, related_name='team_players', on_delete=models.CASCADE)
    national_team = models.ForeignKey(
        Team,
        related_name='national_team_players',
        null=True, blank=True,
        on_delete=models.CASCADE
    )
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    birthday = models.DateField(null=True, blank=True)
    height = models.PositiveSmallIntegerField(default=0)
    weight = models.PositiveSmallIntegerField(default=0)
    shirt_number = models.PositiveSmallIntegerField(default=0)
    nationality = models.ForeignKey(Country, related_name='country_players', on_delete=models.CASCADE)

    class Meta:
        verbose_name = _('Player')
        verbose_name_plural = _('Players')

    def __str__(self):
        return 'Player #{}'.format(self.id)