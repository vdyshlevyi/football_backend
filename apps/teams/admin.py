from django.contrib import admin
from . import models


@admin.register(models.Country)
class CountryAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'name',
        'name_short',
    )

    search_fields = ('name', )

    raw_id_fields = ()
    list_filter = ()


@admin.register(models.League)
class LeagueAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'name',
        'country',
        'members_number',
    )

    search_fields = ('name', )
    raw_id_fields = ('country', )
    list_filter = ()


@admin.register(models.LeagueTeam)
class LeagueTeamAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'league',
        'team',
    )
    raw_id_fields = (
        'team',
        'league',
    )
    list_filter = ()


@admin.register(models.Team)
class TeamAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'country',
        'name',
    )

    search_fields = ('name', )
    raw_id_fields = (
        'country',
    )
    list_filter = ()


@admin.register(models.Player)
class PlayerAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'league',
        'team',
        'first_name',
        'last_name',
        'birthday',
        'height',
        'weight',
        'shirt_number',
        'nationality',
    )

    search_fields = (
        'first_name',
        'last_name',
    )

    raw_id_fields = ('league', 'team', 'nationality', )
    list_filter = ()
