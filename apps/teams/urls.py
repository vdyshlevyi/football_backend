from django.urls import path
from rest_framework.routers import DefaultRouter

from . import views

router = DefaultRouter()
router.register('country', views.CountryViewSet)
router.register('league', views.CountryViewSet)
router.register('team', views.CountryViewSet)
router.register('player', views.CountryViewSet)

urlpatterns = [
   # path('create_player/', views.CreatePlayerView.as_view()),
   # path('player_list/', views.PlayersListView.as_view()),
] + router.urls
