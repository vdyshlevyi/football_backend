from rest_framework import status
from rest_framework.generics import ListAPIView, CreateAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from . import models
from . import serializers


class CountryViewSet(ModelViewSet):
    queryset = models.Country.objects.all()
    serializer_class = serializers.CountrySerializer
    permission_classes = (IsAuthenticated, )

    def partial_update(self, request, *args, **kwargs):
        """ 405 """
        return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)

    def destroy(self, request, *args, **kwargs):
        """ 405 """
        return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)


class LeagueViewSet(ModelViewSet):
    queryset = models.League.objects.all()
    serializer_class = serializers.LeagueSerializer
    permission_classes = (IsAuthenticated, )

    def partial_update(self, request, *args, **kwargs):
        """ 405 """
        return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)

    def destroy(self, request, *args, **kwargs):
        """ 405 """
        return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)


class TeamViewSet(ModelViewSet):
    queryset = models.Team.objects.all()
    serializer_class = serializers.TeamSerializer
    permission_classes = (IsAuthenticated, )

    def partial_update(self, request, *args, **kwargs):
        """ 405 """
        return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)

    def destroy(self, request, *args, **kwargs):
        """ 405 """
        return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)


class PlayerViewSet(ModelViewSet):
    queryset = models.Player.objects.all()
    serializer_class = serializers.PlayerSerializer
    permission_classes = (IsAuthenticated,)

    def partial_update(self, request, *args, **kwargs):
        """ 405 """
        return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)

    def destroy(self, request, *args, **kwargs):
        """ 405 """
        return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)


